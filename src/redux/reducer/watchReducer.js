const data = {
    isStart : false,
    isStartBtn : false,
    isRunning:false,
    sec:0,
    lapArr:[]
}

export function watchReducer(state = data, action) {
    switch(action.type){
        case "START": return {...state, isStartBtn:true, isStart:true, isRunning:true};
        case "STOP" : return {...state, isRunning:false, isStart:false};
        case "LAP" : return {...state, lapArr:[...state.lapArr, state.sec]};
        case "SEC" : return{...state, sec:state.sec+10};
        case "RESET": return {isStartBtn:false, lapArr:[], sec:0, isRunning:false};
        case "RESUME": return {...state,isStart:true, isRunning:true };
        default : return state;
    }
}


