import { combineReducers } from 'redux';
import { watchReducer } from './watchReducer';

export default combineReducers({
    watchProperty : watchReducer
})
