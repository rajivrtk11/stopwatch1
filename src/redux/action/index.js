export function start() {
    return{
        type:"START"
    }
}

export function stop() {
    return{
        type:"STOP"
    }
}

export function reset(){
    return{
        type:"RESET"
    }
}


export function second() {
    return{
        type: "SEC",
    }
}

export function lap() {
    return{
        type: "LAP",
    }
}

export function resume() {
    return{
        type: "RESUME",
    }
}

