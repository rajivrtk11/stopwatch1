import React from 'react';
import { useSelector } from 'react-redux';
import "./Lap.css"

function Lap() {
    const datas = useSelector((state) => state.watchProperty.lapArr);
    return (
        <div className="lap">
            <div className="inner-lap">
                {
                    datas.map(function name(data) {
                        return (
                            <div key={data} className="lap-time">
                                <span className="digits">
                                    {('0' + Math.floor((data / 360000) % 24)).slice(-2)}:
                                </span>
                                <span className="digits">
                                    {('0' + Math.floor((data / 60000) % 60)).slice(-2)}:
                                </span>
                                <span className="digits mili-sec">
                                    {('0' + Math.floor((data / 1000) % 60)).slice(-2)}:
                                </span>
                                <span className="digits mili-sec" style={{ color: 'red' }}>
                                    {('0' + (data / 10) % 100).slice(-2)}
                                </span>
                                {/* {timer} */}
                            </div>
                        )
                    })
                }
            </div>
        </div>
    );
}

export default Lap;