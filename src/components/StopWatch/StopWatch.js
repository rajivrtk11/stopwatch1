import Lap from '../Lap/Lap';
import RunningWatch from '../RunningWatch/RunningWatch';
import "./StopWatch.css"
import { start, stop, reset, lap, resume } from '../../redux/action/index.js';
import { useSelector, useDispatch } from 'react-redux';

function StopWatch() {
    const dispatch = useDispatch();
    const isStart = useSelector((state) => state.watchProperty.isStart)
    const isStartBtn = useSelector((state) => state.watchProperty.isStartBtn)
    const isRunning = useSelector((state) => state.watchProperty.isRunning)
    // const sec = useSelector((state)=> state.watchProperty.sec)
    // const[stoptime, setStoptime] = useState(true);

    return (
        <div>
            <RunningWatch />
            <div>
                <ul id="buttons">
                    {isStartBtn || <li><button onClick={() => dispatch(start())}>Start</button></li>}
                    {(isStartBtn && isRunning) && <>
                        <li><button onClick={() => dispatch(stop())}>Stop</button></li>
                        <li><button onClick={() => dispatch(lap())} >Lap</button></li>
                    </>}
                    {(isStartBtn && !isRunning) && <>
                        <li><button onClick={() => dispatch(resume())}>Resume</button></li>
                        <li><button onClick={() => dispatch(reset())}>Reset</button></li>
                    </>}
                </ul>
            </div>
            <Lap />
        </div>
    );
}

export default StopWatch;

