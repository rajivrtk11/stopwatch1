import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { second } from '../../redux/action/index.js';
import "../StopWatch/StopWatch.css"

function RunningWatch() {
    const sec = useSelector((state) => state.watchProperty.sec);
    const isStart = useSelector((state) => state.watchProperty.isStart)
    const dispatch = useDispatch();

    useEffect(() => {
        if (isStart === true) {
            // console.log("inside useEffect")
            setTimeout(function () {
                dispatch(second())
                console.log();
            }, 10);
        }
    })

    return (
        <div id="stopwatch">
            <span className="digits">
                {('0' + Math.floor((sec / 360000) % 24)).slice(-2)}{isStart ? <span class="separator">:</span> : ":"}
            </span>
            <span className="digits">
                {('0' + Math.floor((sec / 60000) % 60)).slice(-2)}{isStart ? <span class="separator">:</span> : ":"}
            </span>
            <span className="digits mili-props.sec">
                {('0' + Math.floor((sec / 1000) % 60)).slice(-2)}{isStart ? <span class="separator">:</span> : ":"}
            </span>
            <span className="digits mili-props.sec" style={{ color: 'red' }}>
                {('0' + (sec / 10) % 100).slice(-2)}
            </span>
            {/* {timer} */}
        </div>
    );
}

export default RunningWatch;